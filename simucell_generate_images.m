currentPath=pwd;
addpath(genpath([currentPath filesep 'gui']));
addpath(genpath([currentPath filesep 'test']));
addpath(genpath([currentPath filesep 'core']));
addpath(genpath([currentPath filesep 'utils']));
addpath(genpath([currentPath filesep 'plugins']));
addpath(genpath([currentPath filesep '..' filesep 'saved_data']));
simucell_result=[];

version=fileread('version.txt');
disp(['SimuCell version ' version]);

folder_name = uigetdir('.',...
'Pick a folder to save images');

templates = ["normal_cervical","intermediate_cervical","abnormal_cervical"];
num_images_per_template = 1000;
num_digits=floor(log10(num_images_per_template))+1;

for i=1:numel(templates)
    
    template = char(templates(i));
    eval(template)
    
    template_dir = [folder_name filesep template];
    if ~exist(template_dir, 'dir')
        mkdir(template_dir)
    end
    
    for j=1:num_images_per_template
  
        try
            simucell_result=simucell_engine(simucell_data,1);
            
            filename=[ template_dir filesep filesep template '_image_'...
                   sprintf(['%0' num2str(num_digits) 'd'], j) '.jpg'];

            green_channel_img = simucell_result.channel_images.Green;
            imwrite(green_channel_img,filename);

            mask_struct = simucell_result.mask_of_object_by_cell;
            labelmask = simucell_labelmask(mask_struct);
            labelmask_filename = strrep(filename,'.jpg','_mask.txt');
            dlmwrite(labelmask_filename,labelmask, ...
                'delimiter','\t','newline','pc');
            
        catch
            warning(['Error occured on ' template ' image ' num2str(j)])
        end
        
    end
end
%% Script Parameters
IMG_SIZE = [800 800];
NUM_CELLS = 500;
RADIUS = 8;
ECC = 0.5;

%% Script Constants (Doesn't change between normal/abnormal)
RAND = 0.1;
OVERLAP = 0.1;
BASAL_LEVEL = 0.3;

%% Create Subpopulation Cell Array
subpop=cell(0);

%% Define Subpopulation 1
subpop{1}=Subpopulation();

%% Set the Model Placement
subpop{1}.placement=Clustered_Placement();
set(subpop{1}.placement,'number_of_clusters',1);
set(subpop{1}.placement,'cluster_width',IMG_SIZE(1));
set(subpop{1}.placement,'boundary',10);

%% Set the Object Shape
% Object 1
add_object(subpop{1},'Nuc');
subpop{1}.objects.Nuc.model=Nucleus_Model;
set(subpop{1}.objects.Nuc.model,'radius',RADIUS);
set(subpop{1}.objects.Nuc.model,'eccentricity',ECC);
set(subpop{1}.objects.Nuc.model,'randomness',RAND);

%% Define Markers
% Marker 1
add_marker(subpop{1},'Proflavine', Colors.Green);
%% Set Markers Parameters according to the dependencies
op=Constant_Marker_Level();
set(op,'mean_level',0.9);
set(op,'sd_level',0.3);
subpop{1}.markers.Proflavine.Nuc.AddOperation(op);

op=Distance_To_Edge_Marker_Gradient();
set(op,'falloff_radius',5);
set(op,'falloff_type','Exponential');
set(op,'increasing_or_decreasing','Increasing');
subpop{1}.markers.Proflavine.Nuc.AddOperation(op);

%% Set the Composite Type
subpop{1}.compositing=Default_Compositing();
set(subpop{1}.compositing,'container_weight',0);

%% Set Overlap
overlap=Overlap_Specification;
overlap.AddOverlap({subpop{1}.objects.Nuc},OVERLAP);


%% Set Image Artifact
simucell_data.image_artifacts=cell(0);
op=Add_Basal_Brightness();
set(op,'basal_level',BASAL_LEVEL);
simucell_data.image_artifacts{1}=op;


%% Set Image Parameters
simucell_data.subpopulations=subpop;
simucell_data.overlap=overlap;
%Set Number of cell per image
simucell_data.number_of_cells=NUM_CELLS;
%Set Image Size
simucell_data.simucell_image_size=IMG_SIZE;
%Set Population Fraction
simucell_data.population_fractions=[1];
